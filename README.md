# 2tomidnight



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab-student.centralesupelec.fr/ivan.robert/2tomidnight.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab-student.centralesupelec.fr/ivan.robert/2tomidnight/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Name
Our project's name is "2 minutes to Midnight" or "00:00-2".

## Description
The starting point of our project is to reflect about the planetary context (political instability, terrorism, wars, nuclear threats...) and, in particular, the climate emergency. In January 2018, the bulletin of atomic scientists announces that the Doomsday clock has never been so close to midnight (midnight being the end of the world). Today is two minutes to midnight: this scaring time in history gives the project its name. Of course the common thought is that mainly economical, political and environmental institutions are considerate able to make a valuable change in these matters. With this project we tried to look at things from a different perspective, putting art and the artist in the center. In these scaring and particular times, art and beauty are putted aside, they're considered "irrelevant", while the truth is that art and beauty are essential part of our life and we need to protect them. From this observation, some question arise: What can artists and creative people do? Can art save the world? Too pretentious. Can art change the world? Too ambitious. Can art contribute to a positive change in the world? Yes, possibly. Art and its role as a transmitter of values can contribute to a positive change of consciousness. The project was born.

This project consist in the creation of a virtual and a phisycal show that aims to bring a positive change of consciousness, by creating empathetic links with knowledge, by placing the human and the living at the center of the stakes, by awakening consciousness about the beauty of the world (our video at the beginning of the experience aims to do that). Through this awakening and awareness we hope to sensitize the greatest number of people to respect the individual, our eco-system, the living, our planet. And thus, to participate in a more just, more pacifist, more inclusive, and more harmonious world."

The experience is divided into 7 movements or times (physically they are 7 rooms with unique atmospheres) which each invite reflection and introspection. The 7 movements' name and objectives are described as follows:

Movement # 1 - Deep time
The time before and the time after us - this is the channel between the past, the present, the future, accessible through the present moment in constant evolution.
Objective: to show the unity and beauty of a possible harmonious world.

Movement # 2 - Threshold
This movement contains the issues and challenges of the now. It is the urgency of our situation, it is two minutes before the end.
Objective: to show the "Something is wrong" and to alert on the urgency of the current moment in history. 

Movement # 3 - Shedding dissonance
Contemplate the dissonance. We create nothingness, and make space for a new way of being. We let go.
Objective: to invite introspection, self-awareness, silencing the mind to reach a state of being in full awareness.

Movement # 4 - Perspective
To provoke a perceptual and sensory imbalance. This movement is a passage where our perception is reversed. New possibilities unfold in multitudes before us.
Objective: to prepare the reception of emotions, to introduce the notion of other possibilities, to open your eyes on new ways to see the world aroud you.

Movement # 5 - Rhythm of Instincts
To establish a relationship with the living. We evolve in the non-ordinary reality. We trust our instinct and intuition. We embrace our inner animal side.
Objective: to bring out the consciousness of the living inside and outside of us. To raise awareness of the existence of living things, mineral, animal and vegetable

Movement # 6 - Quantum Love
To see beyond matter. We engage in the realm of thought, knowledge and concept. And we open the doors to quantum thinking. Love as a tuner of unity?
Objective: to create empathetic links with knowledge in order to understand the world around us with the enlightenment of knowledge. Putting love at the center of the issues.

Movement # 7 - Quintessence
Engage the inner pendulum that sets the pace for the future. We perceive the authentic and sacred presence of the world. We dance, we celebrate, we co-create.
Objective: to see the world differently, enlightened by an expanded vision, to celebrate the collective and unity to co-create a more beautiful and just world - a world that is peaceful and inclusive. Together, nothing can stop us. 

## Installation
You need first to install nodejs and npm. Then clone the repo and checkout on "vite" branch. Run "nim i vite" command to install vite optimizer. Then run "npm run dev". If an error appears when trying to find a package, run "npm i (name of the package)" then run again "npm run dev".

## Support
For the artistic and descriptive part we referred to Sara, the client of our project and the owner of the exhibition. Also, we had some previous years' documents with some ideas about the project, the movements and the exhibition itself. For the programming part, we referred to Arthur, who is a professional programmer. Overall, the Google Drive was the main support: https://drive.google.com/drive/folders/1f4FT9TQeYZ-JWz3FW4Kcf0EnWYn0f2NY.

## Roadmap
Our project stopped at the completing at the first movement "Deep Time". For the future, it can be useful to continue the development of the other movements, or if needed to improve the first one. 
In order to do that you can consult this file which has all the necessary informations: https://docs.google.com/document/d/10sAR-1doY0ewXQP8qmlD_tc5EXeZu9XEpGhdmHXrKiE/edit.

## Contributing
In order to modify our project you can consult the Google Drive file with all the documentation regarding the project, including our solutions and improvements: https://drive.google.com/drive/u/0/folders/1PLpU0Pndi-6Y7FfeTwG1fpfzfOxVGWBb. 

## Authors and acknowledgment
Firstly, a huge thank to Sara, who was always available and helpful. She followed us every step of the way and she was always really nice and kind every time. Another big thank is reserved to Arthur and Yannick, who both helped technically throughout the whole project

## Project status
This project is only with the 1st movement (to be finished)
1. Write the "salle de thropée" page(list of all the badge and trophy gained in the journey)
2. Write the "Les mouvements" page (just an explanation of each movements).
3. Finish the other movements, and feel free to edit the current ones with your own imagination !
