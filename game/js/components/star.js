import * as THREE from 'three';

export class star {

    //constants
    volumetric = true;
    circle;

    //Variables
    position= new THREE.Vector3(0,0,-500)

    constructor(scene, model, light_power,orbitControl, camera) {
            this.scene = scene
            this.model = model
            this.light_power = light_power
            this.camera = camera
            this.orbitControl = orbitControl
            this.x = this.position.x
            this.y = this.position.y
            this.z = this.position.z
            this.create_light()
            this.create_shape()
        }

    create_light = function() {
        let directionalLight = new THREE.DirectionalLight(0xffccaa,3);
        directionalLight.position.set(0,0,-1);
        this.scene.add(directionalLight)
    }

    create_shape = function() {
        let circleGeo = new THREE.CircleGeometry(100, 50)
        let circleMat = new THREE.MeshBasicMaterial({color: 0xffccaa})
        this.circle = new THREE.Mesh(circleGeo, circleMat)
        this.circle.position.set( this.x,this.y,this.z );
        this.scene.add(this.circle)
    }

    create_rays = function() {
        let godraysEffect = new POST.GodRaysEffect(this.camera, circle, {
        resolutionScale: 1,
        density: 0.8,
        decay: 0.95,
        weight: 0.9,
        samples: 100
      }); 
    }
}