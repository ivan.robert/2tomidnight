import * as THREE from 'three';


export class clicker {
    pointer = new THREE.Vector2();
    raycaster = new THREE.Raycaster();
    intersects = {}
    zero = new THREE.Vector2()
    wanted = {"Great Wall of China" : {"position" : new THREE.Vector3(-0.377930, 0.74694410, 0.7384550), "clicked" : false}, 
    "Corcovado, Rio de Janeiro": {"position" : new THREE.Vector3(0.6649503, -0.8753, -0.175854), "clicked" : false},
     "Mount Everest":{"position" : new THREE.Vector3(-0.02529,0.97843 , 0.533682), "clicked" : false}, 
     "Petra Jordan":{"position" : new THREE.Vector3(0.77493, 0.58858, 0.54388), "clicked" : false}, 
     "Machu Picchu":{"position" : new THREE.Vector3(0.285401,-1.068470 ,-0.126802 ), "clicked" : false}, 
     "Australia":{"position" : new THREE.Vector3(-0.689013,0.74333 ,-0.46307 ), "clicked" : false}, 
     "Grand Canyon":{"position" : new THREE.Vector3(-0.23753, -0.897255, 0.618439), "clicked" : false},
     "African Savannah":{"position" : new THREE.Vector3(1.0564177, 0.311030, 0.16339272), "clicked" : false}}


    constructor(camera, scene, model){
        this.camera = camera;
        this.scene = scene;
        this.model = model

    }
    onMM = function(event) { //update the cursor's position on mouse move
        this.pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
        this.pointer.y = -(event.clientY / window.innerHeight) * 2 + 1;
    }
    onClick = function() { //Place a cube on the intersection with the first object hit by the casted ray
        this.raycaster.setFromCamera(this.pointer, this.camera)
        this.intersects = this.raycaster.intersectObjects(this.scene.children)
        if (this.intersects.length>0) {
            const intersection = this.intersects[0].point
            const Lintersection = this.model.children[0].worldToLocal(intersection)
            this.place_cube(Lintersection)
            this.get_minimum_distance(Lintersection)

        }
    }
    place_cube = function (position) { //places a cube and makes it rotate on earth
        const sphereGeometry = new THREE.SphereGeometry(0.02,20,20)
        const boxMaterial = new THREE.MeshPhongMaterial({color: 0xff0000, emissive: 0xff0000})
        const boxMesh = new THREE.Mesh(sphereGeometry, boxMaterial)
        boxMesh.position.set(position.x, position.y, position.z)
        this.model.children[0].add(boxMesh)

    }

    get_minimum_distance = function(clicked) { //Fetches the closest place that has not been clicked yet in this.wanted
        const keys = Object.keys(this.wanted)
        var min_dist = 5; //angular distance greater than pi
        var place = "No place"
        for (let i = 0; i < keys.length; i++) {
            const dist = clicked.angleTo(this.wanted[keys[i]]["position"])
            if (!this.wanted[keys[i]]["clicked"] && dist < min_dist) {
                min_dist = dist
                if (place != "No place") {
                    this.wanted[place]["clicked"] = false
                }
                place = keys[i]
                this.wanted[place]["clicked"] = true
            }
        }
        console.log("Remaining closest place : " + place + " with a distance of : " + min_dist)
    }


}