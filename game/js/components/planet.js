import * as THREE from 'three';

export class planet {

    rotate = true

    //constants
    planet_speed= 0.001

    constructor(model, scene) {
        this.scene = scene;
        this.model = model;
        this.scene.add(model)
    }
    // compute earth rotation
    update = function() {
        if (this.rotate) {
        this.model.children[0].rotateZ(this.planet_speed)
        }
    }
}