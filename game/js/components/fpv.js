import * as THREE from 'three';


export class FPV {


    //constants
    cam_max_speed=0.05
    slowfactor=1.1
    sensitivity=0.0005



    //Variables
    angle =0
    cam_speed= 0



    constructor(model, orbitControl, camera) {
            this.model = model
            this.camera = camera
            this.orbitControl = orbitControl
            this.camera.position.x += 0
            this.camera.position.z += 0
        }



    update = function(keysPressed) {

        //get camera horizontal position
        const xcam = this.camera.position.x;
        const zcam = this.camera.position.z;
        const xpla = this.model.position.x;
        const zpla = this.model.position.z;

        //compute planar distance
        const dist = Math.sqrt(Math.pow(xcam-xpla, 2) + Math.pow(zcam-zpla, 2))

        //change angular speed on keypress
        if (keysPressed['d']) {
            if (Math.abs(this.cam_speed)<this.cam_max_speed) {
                this.cam_speed-=0.001
            }
        }
        if (keysPressed['q']) {
            if (Math.abs(this.cam_speed)<this.cam_max_speed) {
                this.cam_speed+=0.001
            }
        }

        //decrease angular speed when no key is pressed
        if (Math.abs(this.cam_speed)>0 && !keysPressed['q'] && !keysPressed['d']) {
            this.cam_speed= this.cam_speed/this.slowfactor
        }
        //Compute next angular position
        this.angle+=this.cam_speed;

        //Convert angular position to 2d position and give it to the camera
        this.camera.position.x += Math.cos(this.angle) * dist - this.camera.position.x
        this.camera.position.z += Math.sin(this.angle) * dist - this.camera.position.z
    }



    //implementing fpv using the target of orbitcontrols
    movetarget = function(x,y,z) {

        //Get positions of camera and planet
        var cam = new THREE.Vector3(this.camera.position.x,this.camera.position.y,this.camera.position.z)
        var planet = new THREE.Vector3(this.model.position.x,this.model.position.y,this.model.position.z)

        //Create local camera reference space
        var localZ = planet.clone();
        localZ.sub(cam);
        localZ.normalize();
        const X = new THREE.Vector3(1,0,0)
        const Y = new THREE.Vector3(0,1,0)
        const Z = new THREE.Vector3(0,0,1)
        if (localZ.angleTo(X)<0.01 || Math.abs(localZ.angleTo(X)-Math.PI) <0.01){
            var localY = new THREE.Vector3
            localY.crossVectors(Z,localZ)
            localY.normalize()
        } else{
            var localY = new THREE.Vector3
            localY.crossVectors(X,localZ)
            localY.normalize()
        }
        if (localY.dot(Y)<0){
            localY.multiplyScalar(-1)
        }
        var localX = new THREE.Vector3
        localX.crossVectors(localZ,localY)
        localX.normalize()

        //Move the camera acordingly in the newly created space
        this.orbitControl.target.add(localX.multiplyScalar(x));
        this.orbitControl.target.add(localY.multiplyScalar(y));
        this.orbitControl.target.add(localZ.multiplyScalar(z))

    }
}