import * as THREE from "three";
import {OrbitControls} from "orbitcontrols";
import {FirstPersonControls} from "fpc";
import {GLTFLoader} from "GLTFLoader" ;
// import { FPV, planet, star, clicker } from "./components.js";
import {clicker} from "./components/clicker.js";
import {star} from "./components/star.js";
import {planet} from "./components/planet.js";
import {FPV} from "./components/fpv.js";


function scene_start() {



    // Initialize scene
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 0.1, 1000 );
    const renderer = new THREE.WebGLRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );
    camera.position.z = 2;



    // Initialize controls
    const controls = new OrbitControls( camera, renderer.domElement );
    controls.enabled = false;  //Controls cannot be controlled with mouse clicks
    controls.enableDamping = true;
    controls.update();



    //Importing earth and initializing controls
    var earth
    var fpv
    var pointer
    var loader = new GLTFLoader();
    loader.load('./ressource/earth/scene.gltf',  ( gltf ) => {
        const model = gltf.scene
        model.children[0].scale.set(1,1,1)
        earth = new planet(model, scene);
        fpv = new FPV(earth.model, controls, camera)
        pointer = new clicker(camera, scene, earth.model)
    });



    //Sun
    var sun
    sun = new star(scene, null, 0.8, controls, camera);



    //Space texture
    const skypath = './ressource/cube_space/space_'
    var skybox = new THREE.CubeTextureLoader().load([
        skypath + 'front.jpg', //front
        skypath + 'back.jpg', //back
        skypath + 'top.jpg', //top
        skypath + 'bottom.jpg',  //bottom
        skypath + 'left.jpg', //left
        skypath + 'right.jpg', //right
    ])
    scene.background = skybox



    //Ambiant light
    const lcolor = 0xFFFFFF;
    const lintensity = 0.8;
    const light = new THREE.AmbientLight(lcolor, lintensity);
    scene.add(light);



    // CONTROL KEYS
    const keysPressed = {  }  //when key pressed, dictionnary[key] == true
    document.addEventListener('keydown', (event) => {
        keysPressed[event.key.toLowerCase()] = true
    }, false);
    document.addEventListener('keyup', (event) => {
        keysPressed[event.key.toLowerCase()] = false
    }, false);



    //FPV CONTROL
    var xmouse = 0;
    var ymouse = 0;
    document.addEventListener('mousemove', (event) => {
        xmouse = event.clientX
        ymouse = event.clientY
        if(earth){
            pointer.onMM(event)
    }}, false);

    function move_around(vector) {
        if (vector.distanceTo(pointer.zero) > 0.01) {
            fpv.movetarget(vector.x*fpv.sensitivity*350,vector.y*fpv.sensitivity*350,0);
        }
    }

    document.addEventListener('click', (event) => {
        if(earth && (pointer.model.children[0].children.length - 1) < Object.keys(pointer.wanted).length){
            pointer.onClick()
        }
    }, false);



    //Recenter function
    function recenter() {
        if(earth){
            if (fpv.orbitControl.target.distanceTo(earth.model.position)>0.001 || keysPressed[' '] || keysPressed['q'] || keysPressed['d']) {
                fpv.orbitControl.target.addScaledVector( fpv.orbitControl.target, -0.1);
            }
        }
}



    //Window size adaptation
    window.addEventListener('resize', () => {
        camera.aspect = window.innerWidth/window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight)
    }, false)

    var isPaused = false
    //pause
    window.addEventListener("keydown", function(event) {
    if(event.key === "Escape") {
        isPaused = !isPaused;
        if (!isPaused) {
            requestAnimationFrame( animate );
        } 
}
})

    //Animation
    const clock = new THREE.Clock;
    function animate() {
        if (!isPaused) {
            requestAnimationFrame( animate );
        }
        var delta = clock.getDelta();
        controls.update(delta);
        if(earth) {
        fpv.update(keysPressed);
        earth.update();
        move_around(pointer.pointer);
        recenter();
        }
        // console.log(isPaused)
        // move_around(xmouse, ymouse);
        renderer.render( scene, camera );
    };

    animate();
}
export { scene_start }