import cv2 as cv
import numpy as np
from scipy import ndimage

# TO COMPLETE :
space = cv.imread('C:/Users/ivanr/Desktop/Notes/2A/Projet/tests_code/2tomidnight/ressources/space_1.jpg') #Replace path by the one of your image
top_pos = 3 #Replace by horizontal position (/!\ between 0 and 3 /!\ ) of top and bottom

# Blur
# space = cv.blur(space,(10,10))


size = len(space)//3
round = space[size:2*size]
faces = ['right', 'back', 'left', 'front']
faces[top_pos] = 'front'
faces[(top_pos+2)%4] = 'back'
faces[(top_pos+1)%4] = 'right'
faces[(top_pos-1)%4] = 'left'
round_faces = []
for i in range(4):
    round_faces.append(np.array([ligne[size*i:size*(i+1)] for ligne in round]))
    cv.imwrite('C:/Users/ivanr/Desktop/Notes/2A/Projet/tests_code/2tomidnight/ressources/cube_space/space_'+ faces[i] + '.jpg', round_faces[i])

# Top and bottom
top = space[0:640]
top = np.array([ligne[top_pos*size:size*(top_pos+1)] for ligne in top])
bottom = space[2*size:3*size]
bottom = np.array([ligne[top_pos*size:size*(top_pos+1)] for ligne in bottom])
top = ndimage.rotate(top, 90)
bottom = ndimage.rotate(bottom, -90)

cv.imwrite('C:/Users/ivanr/Desktop/Notes/2A/Projet/tests_code/2tomidnight/ressources/cube_space/space_top.jpg', top)
cv.imwrite('C:/Users/ivanr/Desktop/Notes/2A/Projet/tests_code/2tomidnight/ressources/cube_space/space_bottom.jpg', bottom)

